package ru.tac.anystat.javarest;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

/**
 * Created by ustits on 28.09.16.
 */
@SpringBootApplication
@ComponentScan
@EnableJpaRepositories(basePackages = "ru.tac.anystat.javarest")
public class Application {

    public static void main(String[] args) {
        SpringApplication.run(Application.class, args);
    }
}
