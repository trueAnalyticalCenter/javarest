package ru.tac.anystat.javarest.repositories;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.CrudRepository;
import ru.tac.anystat.javarest.model.Keyword;

/**
 * Created by ustits on 06.10.16.
 */
public interface KeywordRepository extends CrudRepository<Keyword, Integer> {

    Page<Keyword> findAll(Pageable pageable);
}
