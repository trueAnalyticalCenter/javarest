package ru.tac.anystat.javarest.repositories;

import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.transaction.annotation.Transactional;
import ru.tac.anystat.javarest.model.Page;

/**
 * Created by ustits on 06.10.16.
 */
public interface PageRepository extends CrudRepository<Page, Integer> {

    @Override
    @Transactional(timeout = 10)
    Iterable<Page> findAll();

    org.springframework.data.domain.Page<Page> findAll(Pageable pageable);

    @Query("select p from Page p where p.url not like '%.xml' " +
            "and p.url not like '%.xml.gz' " +
            "and p.url not like '%/robots.txt'")
    org.springframework.data.domain.Page<Page> findAllPlainPages(Pageable pageable);
}
