package ru.tac.anystat.javarest.repositories;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.CrudRepository;
import ru.tac.anystat.javarest.model.Person;

/**
 * Created by ustits on 06.10.16.
 */
public interface PersonRepository extends CrudRepository<Person, Integer> {

    Person findByName(String name);

    Page<Person> findAll(Pageable pageable);
}
