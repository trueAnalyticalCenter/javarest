package ru.tac.anystat.javarest.repositories;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.transaction.annotation.Transactional;
import ru.tac.anystat.javarest.model.Site;

/**
 * Created by ustits on 29.09.16.
 */
public interface SiteRepository extends CrudRepository<Site, Integer> {

    Site findByName(String name);

    @Modifying
    @Transactional
    @Query("delete from Site s where s.name = ?1")
    void deleteByName(String name);

    Page<Site> findAll(Pageable pageable);
}
