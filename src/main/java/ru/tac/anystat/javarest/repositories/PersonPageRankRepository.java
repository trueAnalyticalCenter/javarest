package ru.tac.anystat.javarest.repositories;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.transaction.annotation.Transactional;
import ru.tac.anystat.javarest.model.PersonPageRank;

/**
 * Created by ustits on 06.10.16.
 */
public interface PersonPageRankRepository extends CrudRepository<PersonPageRank, Integer> {

    @Override
    @Transactional(timeout = 10)
    Iterable<PersonPageRank> findAll();

    Page<PersonPageRank> findAll(Pageable pageable);

    Page<PersonPageRank> findAllByPersonId(Integer person, Pageable pageable);

    @Query("select sum(p.rank) from PersonPageRank p " +
            "where p.person.id = ?1")
    Long totalRank(Integer id);

    @Query("select sum(p.rank) from PersonPageRank p " +
            "where p.person.id = ?1 and p.page.site.id = ?2")
    Long totalRankBySite(Integer personId, Integer pageId);

}
