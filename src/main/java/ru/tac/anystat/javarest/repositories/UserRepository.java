package ru.tac.anystat.javarest.repositories;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.CrudRepository;
import ru.tac.anystat.javarest.model.User;

/**
 * Created by ustits on 06.10.16.
 */
public interface UserRepository extends CrudRepository<User, Integer> {

    User findByUsername(String username);

    Page<User> findAll(Pageable pageable);
}
