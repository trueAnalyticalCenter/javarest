package ru.tac.anystat.javarest.controllers;

import org.springframework.data.domain.PageRequest;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import ru.tac.anystat.javarest.controllers.exceptions.NoSuchEntityException;
import ru.tac.anystat.javarest.model.Keyword;
import ru.tac.anystat.javarest.model.Person;
import ru.tac.anystat.javarest.repositories.KeywordRepository;

import java.util.Map;

/**
 * Created by ustits on 06.10.16.
 */
@RestController
@RequestMapping("/keywords")
public class KeywordRestController extends AbstractController<KeywordRepository, Keyword> {

    private static final String NAME_FIELD = "name";
    private static final String PERSON_ID_FIELD = "person_id";

    private String name;
    private String personId;

    @Override
    public Iterable<Keyword> getAll(@PathVariable Integer page) {
        return getRepository().findAll(new PageRequest(page, ENTRIES_COUNT));
    }

    @Override
    public Iterable<Keyword> getAll() {
        return getRepository().findAll(new PageRequest(0, ENTRIES_COUNT));
    }

    @Override
    public ResponseEntity<Keyword> addEntity(@RequestParam Map<String, String> requestParams) throws Exception {
        setFields(requestParams);
        checkMandatoryFields(name, personId);

        Person person = new Person();
        person.setId(Integer.valueOf(personId));
        Keyword keyword = new Keyword();
        keyword.setName(name);
        keyword.setPerson(person);
        getRepository().save(keyword);
        return new ResponseEntity<>(keyword, HttpStatus.OK);
    }

    @Override
    public ResponseEntity<Keyword> updateEntity
            (@PathVariable Integer id, @RequestParam Map<String, String> requestParams) throws Exception {
        setFields(requestParams);
        Keyword keyword = getRepository().findOne(id);
        if (keyword == null)
            throw new NoSuchEntityException();

        if (name != null)
            keyword.setName(name);

        getRepository().save(keyword);
        return new ResponseEntity<>(keyword, HttpStatus.OK);
    }

    @Override
    protected void setFields(Map<String, String> requestParams) {
        name = requestParams.get(NAME_FIELD);
        personId = requestParams.get(PERSON_ID_FIELD);
    }
}
