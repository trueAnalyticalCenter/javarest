package ru.tac.anystat.javarest.controllers;

import org.springframework.data.domain.PageRequest;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import ru.tac.anystat.javarest.controllers.exceptions.NoSuchEntityException;
import ru.tac.anystat.javarest.model.Person;
import ru.tac.anystat.javarest.model.User;
import ru.tac.anystat.javarest.repositories.PersonRepository;

import java.util.Map;

/**
 * Created by ustits on 06.10.16.
 */
@RestController
@RequestMapping("/persons")
public class PersonRestController extends AbstractController<PersonRepository, Person> {

    private static final String NAME_FIELD = "name";
    private static final String ADMIN_ID_FIELD = "user_id";

    private String name;
    private String adminId;

    @Override
    public Iterable<Person> getAll(@PathVariable Integer page) {
        return getRepository().findAll(new PageRequest(page, ENTRIES_COUNT));
    }

    @Override
    public Iterable<Person> getAll() {
        return getRepository().findAll(new PageRequest(0, ENTRIES_COUNT));
    }

    @Override
    public ResponseEntity<Person> addEntity(@RequestParam Map<String, String> requestParams)
            throws Exception {
        setFields(requestParams);
        checkMandatoryFields(name);

        Person person = getRepository().findByName(name);
        if (person == null) {
            person = new Person();
            person.setName(name);
        }
        if (adminId != null) {
            User admin = new User();
            admin.setId(Integer.valueOf(adminId));
            person.setAddedBy(admin);
        }

        getRepository().save(person);
        return new ResponseEntity<>(person, HttpStatus.OK);
    }

    @Override
    public ResponseEntity<Person> updateEntity
            (@PathVariable Integer id, @RequestParam Map<String, String> requestParams) throws Exception {
        setFields(requestParams);
        Person person = getRepository().findOne(id);
        if (person == null)
            throw new NoSuchEntityException();
        if (name != null)
            person.setName(name);
        getRepository().save(person);
        return new ResponseEntity<>(person, HttpStatus.OK);
    }

    @Override
    protected void setFields(Map<String, String> requestParams) {
        name = requestParams.get(NAME_FIELD);
        adminId = requestParams.get(ADMIN_ID_FIELD);
    }
}
