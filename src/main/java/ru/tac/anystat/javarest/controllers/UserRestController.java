package ru.tac.anystat.javarest.controllers;

import org.springframework.data.domain.PageRequest;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import ru.tac.anystat.javarest.controllers.exceptions.NoSuchEntityException;
import ru.tac.anystat.javarest.model.User;
import ru.tac.anystat.javarest.repositories.UserRepository;

import java.util.Map;

/**
 * Created by ustits on 06.10.16.
 */
@RestController
@RequestMapping("/users")
public class UserRestController extends AbstractController<UserRepository, User> {

    private static final String USERNAME_FIELD = "name";
    private static final String PASSWORD_FIELD = "pass";
    private static final String IS_ADMIN_FIELD = "admin";
    private static final String ADMIN_ID_FIELD = "admin_id";

    private String username;
    private String password;
    private String isAdmin;
    private String creator;

    @Override
    public Iterable<User> getAll(@PathVariable Integer page) {
        Integer pageNum = Integer.valueOf(page);
        return getRepository().findAll(new PageRequest(pageNum, ENTRIES_COUNT));
    }

    @Override
    public Iterable<User> getAll() {
        return getRepository().findAll(new PageRequest(0, ENTRIES_COUNT));
    }

    @Override
    public ResponseEntity<User> addEntity(@RequestParam Map<String, String> requestParams) throws Exception {
        setFields(requestParams);
        checkMandatoryFields(username, password);

        User user = new User();
        user.setUsername(username);
        user.setPassword(password);

        // if there is no entry about admin than we make a simple user
        if (isAdmin == null)
            user.setIsAdmin(false);
        else {
            user.setIsAdmin(isAdmin(isAdmin));
        }

        if (creator != null) {
            User creatorEntity = getRepository().findOne(Integer.valueOf(creator));
            if (creatorEntity != null)
                user.setCreatedBy(creatorEntity);
        }

        getRepository().save(user);

        return new ResponseEntity<>(user, HttpStatus.OK);
    }

    @Override
    public ResponseEntity<User> updateEntity
            (@PathVariable Integer id, @RequestParam Map<String, String> requestParams) throws Exception {
        setFields(requestParams);
        User user = getRepository().findOne(id);
        if (user == null)
            throw new NoSuchEntityException();
        if (username != null)
            user.setUsername(username);
        if (password != null)
            user.setPassword(password);
        if (isAdmin != null) {
            user.setIsAdmin(isAdmin(isAdmin));
        }
        getRepository().save(user);
        return new ResponseEntity<>(user, HttpStatus.OK);
    }

    @Override
    protected void setFields(Map<String, String> requestParams) {
        username = requestParams.get(USERNAME_FIELD);
        password = requestParams.get(PASSWORD_FIELD);
        isAdmin = requestParams.get(IS_ADMIN_FIELD);
        creator = requestParams.get(ADMIN_ID_FIELD);
    }

    private boolean isAdmin(String admin) {
        int tmp = Integer.valueOf(admin);
        Boolean isAdmin = false;
        if (tmp == 1) isAdmin = true;
        return isAdmin;
    }
}
