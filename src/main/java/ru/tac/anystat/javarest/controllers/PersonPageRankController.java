package ru.tac.anystat.javarest.controllers;

import net.minidev.json.JSONObject;
import net.minidev.json.JSONUtil;
import org.springframework.data.domain.PageRequest;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import ru.tac.anystat.javarest.model.Page;
import ru.tac.anystat.javarest.model.Person;
import ru.tac.anystat.javarest.model.PersonPageRank;
import ru.tac.anystat.javarest.repositories.PersonPageRankRepository;
import ru.tac.anystat.javarest.utils.JsonUtils;

import java.util.Map;

/**
 * Created by ustits on 06.10.16.
 */
@RestController
@RequestMapping("/ranks")
public class PersonPageRankController extends AbstractController<PersonPageRankRepository, PersonPageRank> {

    private static final String PERSON_ID_FIELD= "person_id";
    private static final String PAGE_ID_FIELD = "page_id";
    private static final String RANK_FIELD = "rank";

    private String personId;
    private String pageId;
    private String rank;

    @Override
    public Iterable<PersonPageRank> getAll(@PathVariable Integer page) {
        return getRepository().findAll(new PageRequest(page, ENTRIES_COUNT));
    }

    @Override
    public Iterable<PersonPageRank> getAll() {
        return getRepository().findAll(new PageRequest(0, ENTRIES_COUNT));
    }

    @RequestMapping(value = "/person/{person}", method = RequestMethod.GET)
    public Iterable<PersonPageRank> getAllByPerson(@PathVariable Integer person) {
        return getRepository().findAllByPersonId(person, new PageRequest(0, ENTRIES_COUNT));
    }

    @RequestMapping(value = "/person/{person}/page/{page}", method = RequestMethod.GET)
    public Iterable<PersonPageRank> getAllByPerson(@PathVariable Integer person, @PathVariable Integer page) {
        return getRepository().findAllByPersonId(person, new PageRequest(page, ENTRIES_COUNT));
    }

    @Override
    public ResponseEntity<PersonPageRank> addEntity(@RequestParam Map<String, String> requestParams)
            throws Exception {
        setFields(requestParams);
        checkMandatoryFields(personId, pageId, rank);

        PersonPageRank rankEntity = new PersonPageRank();
        rankEntity.setRank(Integer.valueOf(rank));

        Person person = new Person();
        person.setId(Integer.valueOf(personId));
        rankEntity.setPerson(person);

        Page page = new Page();
        page.setId(Integer.valueOf(pageId));
        rankEntity.setPage(page);

        getRepository().save(rankEntity);

        return new ResponseEntity<>(rankEntity, HttpStatus.OK);
    }


    @Override
    protected void setFields(Map<String, String> requestParams) {
        personId = requestParams.get(PERSON_ID_FIELD);
        pageId = requestParams.get(PAGE_ID_FIELD);
        rank = requestParams.get(RANK_FIELD);
    }

    @RequestMapping(value = "/total/person/{id}", method = RequestMethod.GET)
    public ResponseEntity<String> getTotalRankByPerson(@PathVariable Integer id) {
        Long value = getRepository().totalRank(id);
        return JsonUtils.getTotalRank(id, value);
    }

    @RequestMapping(value = "/total/person/{personId}/site/{siteId}", method = RequestMethod.GET)
    public ResponseEntity<String> getTotalRankByPersonAndSite(@PathVariable Integer personId,
                                            @PathVariable Integer siteId) {
        Long value = getRepository().totalRankBySite(personId, siteId);
        return JsonUtils.getTotalRank(personId, siteId, value);
    }
}
