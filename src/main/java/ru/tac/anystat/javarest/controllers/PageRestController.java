package ru.tac.anystat.javarest.controllers;

import org.springframework.data.domain.PageRequest;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import ru.tac.anystat.javarest.controllers.exceptions.NoSuchEntityException;
import ru.tac.anystat.javarest.model.Page;
import ru.tac.anystat.javarest.model.Site;
import ru.tac.anystat.javarest.repositories.PageRepository;
import ru.tac.anystat.javarest.utils.DateUtils;
import ru.tac.anystat.javarest.utils.JsonUtils;

import java.sql.Timestamp;
import java.text.ParseException;
import java.util.Map;

/**
 * Created by ustits on 06.10.16.
 */
@RestController
@RequestMapping("/pages")
public class PageRestController extends AbstractController<PageRepository, Page> {

    private static final String URL_FIELD = "url";
    private static final String FOUND_DATE_FIELD = "found_date";
    private static final String LAST_DATE_FIELD = "last_date";
    private static final String MOD_DATE_FIELD = "mod_date";
    private static final String SITE_ID_FIELD = "site_id";

    private String url;
    private String foundDate;
    private String lastDate;
    private String modDate;
    private String siteId;

    @Override
    public Iterable<Page> getAll(@PathVariable Integer page) {
        return getRepository().findAllPlainPages(new PageRequest(page, ENTRIES_COUNT));
    }

    @Override
    public Iterable<Page> getAll() {
        return getRepository().findAllPlainPages(new PageRequest(0, ENTRIES_COUNT));
    }

    @Override
    public ResponseEntity<Page> addEntity(@RequestParam Map<String, String> requestParams) throws Exception {
        setFields(requestParams);
        checkMandatoryFields(url, foundDate, siteId);

        Page page = new Page();
        page.setUrl(url);

        setDates(page);

        Site site = new Site(Integer.valueOf(siteId));
        page.setSite(site);
        getRepository().save(page);
        return new ResponseEntity<>(page, HttpStatus.OK);
    }

    @Override
    public ResponseEntity<Page> updateEntity
            (@PathVariable Integer id, @RequestParam Map<String, String> requestParams) throws Exception {
        setFields(requestParams);
        Page page = getRepository().findOne(id);

        if (page == null)
            throw new NoSuchEntityException();

        if (url != null)
            page.setUrl(url);

        return new ResponseEntity<>(page, HttpStatus.OK);
    }

    @Override
    protected void setFields(Map<String, String> requestParams) {
        url = requestParams.get(URL_FIELD);
        foundDate = requestParams.get(FOUND_DATE_FIELD);
        lastDate = requestParams.get(LAST_DATE_FIELD);
        modDate = requestParams.get(MOD_DATE_FIELD);
        siteId = requestParams.get(SITE_ID_FIELD);
    }

    @ExceptionHandler(ParseException.class)
    public ResponseEntity handleParseError() {
        return JsonUtils.getErrorEntity(400,
                "Can't parse date", HttpStatus.BAD_REQUEST);
    }

    private void setDates(Page page) throws ParseException {
        Timestamp foundDateTime = DateUtils.convertToTimestamp(foundDate);
        page.setFoundDateTime(foundDateTime);
        if (lastDate != null) {
            Timestamp lastScanDate = DateUtils.convertToTimestamp(lastDate);
            page.setLastScanDate(lastScanDate);
        }
        if (modDate != null) {
            Timestamp modScanDate = DateUtils.convertToTimestamp(modDate);
            page.setLastScanDate(modScanDate);
        }
    }
}
