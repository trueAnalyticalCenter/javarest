package ru.tac.anystat.javarest.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.repository.CrudRepository;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import ru.tac.anystat.javarest.controllers.exceptions.MandatoryFieldException;
import ru.tac.anystat.javarest.controllers.exceptions.NoSuchEntityException;
import ru.tac.anystat.javarest.utils.JsonUtils;

import java.util.Map;

/**
 * Created by ustits on 09.10.16.
 */
public abstract class AbstractController<T extends CrudRepository, U> {

    protected final static Integer ENTRIES_COUNT = 100;

    @Autowired
    private T repository;

    @RequestMapping(value = "/add", method = RequestMethod.POST)
    public abstract ResponseEntity<U> addEntity(@RequestParam Map<String, String> requestParams) throws Exception;

    @RequestMapping(value = "/page/{page}", method = RequestMethod.GET)
    public abstract Iterable<U> getAll(@PathVariable Integer page);

    @RequestMapping(method = RequestMethod.GET)
    public abstract Iterable<U> getAll();

    @RequestMapping(value = "/update/{id}", method = RequestMethod.POST)
    public ResponseEntity<U> updateEntity
            (@PathVariable Integer id,
             @RequestParam Map<String, String> requestParams) throws Exception {
           throw new NoSuchMethodException("Update");
    }

    @RequestMapping(value = "{id}", method = RequestMethod.GET)
    public U getEntityById(@PathVariable Integer id) {
        return (U)repository.findOne(id);
    }

    @RequestMapping(value = "/delete/{id}", method = RequestMethod.POST)
    public ResponseEntity deleteEntity(@PathVariable Integer id) throws Exception {
        Object entity = repository.findOne(id);
        if (entity == null) throw new NoSuchEntityException();
        repository.delete(id);
        return new ResponseEntity(HttpStatus.OK);
    }

    @ExceptionHandler(MandatoryFieldException.class)
    public ResponseEntity handleMandatoryError() {
        return JsonUtils.getErrorEntity(400,
                "Mandatory fields were not set", HttpStatus.BAD_REQUEST);
    }

    @ExceptionHandler(NoSuchEntityException.class)
    public ResponseEntity handleNoEntityError() {
        return JsonUtils.getErrorEntity(400,
                "No entity with such id found", HttpStatus.BAD_REQUEST);
    }

    @ExceptionHandler(NoSuchMethodException.class)
    public ResponseEntity handleNoMethodError(NoSuchMethodException e) {
        return JsonUtils.getErrorEntity(400,
                e.getMessage() + " method is not supported", HttpStatus.BAD_REQUEST);
    }

    protected abstract void setFields(Map<String, String> requestParams);

    protected void checkMandatoryFields(String... fields) throws MandatoryFieldException {
        for (String field : fields) {
            if (field == null || field.isEmpty())
                throw new MandatoryFieldException();
        }
    }

    protected T getRepository() {
        return repository;
    }
}
