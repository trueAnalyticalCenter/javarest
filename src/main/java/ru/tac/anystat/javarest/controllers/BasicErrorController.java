package ru.tac.anystat.javarest.controllers;

import org.springframework.boot.autoconfigure.web.AbstractErrorController;
import org.springframework.boot.autoconfigure.web.ErrorAttributes;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import ru.tac.anystat.javarest.utils.JsonUtils;

import javax.servlet.http.HttpServletRequest;

/**
 * Created by ustits on 21.10.16.
 */
@RestController
public class BasicErrorController extends AbstractErrorController {

    public BasicErrorController(ErrorAttributes errorAttributes) {
        super(errorAttributes);
    }

    @RequestMapping(value = "/error")
    public ResponseEntity customError(HttpServletRequest request) {
        HttpStatus status = getStatus(request);
        return JsonUtils.getErrorEntity(status.value(), status.getReasonPhrase(), status);
    }

    @Override
    public String getErrorPath() {
        return "/error";
    }
}
