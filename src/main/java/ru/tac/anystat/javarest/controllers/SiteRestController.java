package ru.tac.anystat.javarest.controllers;

import org.springframework.data.domain.PageRequest;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import ru.tac.anystat.javarest.controllers.exceptions.NoSuchEntityException;
import ru.tac.anystat.javarest.model.Site;
import ru.tac.anystat.javarest.model.User;
import ru.tac.anystat.javarest.repositories.SiteRepository;

import java.util.Map;

/**
 * Created by ustits on 28.09.16.
 */
@RestController
@RequestMapping("/sites")
public class SiteRestController extends AbstractController<SiteRepository, Site>{

    private static final String NAME_FIELD = "name";
    private static final String USER_ID_FIELD = "user_id";

    private String name;
    private String userId;

    @Override
    public Iterable<Site> getAll(@PathVariable Integer page) {
        return getRepository().findAll(new PageRequest(page, ENTRIES_COUNT));
    }

    @Override
    public Iterable<Site> getAll() {
        return getRepository().findAll(new PageRequest(0, ENTRIES_COUNT));
    }

    @Override
    public ResponseEntity<Site> addEntity(@RequestParam Map<String, String> requestParams) throws Exception {
        setFields(requestParams);
        checkMandatoryFields(name);

        Site site = getRepository().findByName(name);
        if (site == null) {
            site = new Site();
            site.setName(name);
        }

        if (userId != null) {
            User user = new User(Integer.valueOf(userId));
            site.setAddedBy(user);
        }
        getRepository().save(site);
        return new ResponseEntity<>(site, HttpStatus.OK);
    }

    @Override
    public ResponseEntity<Site> updateEntity(@PathVariable Integer id, @RequestParam Map<String, String> requestParams) throws Exception {
        setFields(requestParams);
        Site site = getRepository().findOne(id);
        if (site == null)
            throw new NoSuchEntityException();
        if (name != null)
            site.setName(name);
        getRepository().save(site);
        return new ResponseEntity<>(site, HttpStatus.OK);
    }

    @Override
    protected void setFields(Map<String, String> requestParams) {
        name = requestParams.get(NAME_FIELD);
        userId = requestParams.get(USER_ID_FIELD);
    }
}
