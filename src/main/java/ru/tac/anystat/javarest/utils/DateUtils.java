package ru.tac.anystat.javarest.utils;

import java.sql.Timestamp;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Created by ustits on 07.10.16.
 */
public class DateUtils {

    public static Timestamp convertToTimestamp(String time) throws ParseException {
//        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss.SSS");
        SimpleDateFormat dateFormat = new SimpleDateFormat("dd.MM.yyyy");
        Date parsedDate = dateFormat.parse(time);
        return new Timestamp(parsedDate.getTime());
    }
}
