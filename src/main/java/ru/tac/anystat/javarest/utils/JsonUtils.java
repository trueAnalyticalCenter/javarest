package ru.tac.anystat.javarest.utils;

import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

/**
 * Created by ustits on 22.10.16.
 */
public class JsonUtils {

    private static final String JSON_ERROR =
            "{\"error\":\"400\",\"message\":\"Can't form json error\"}";

    public static ResponseEntity getErrorEntity(Integer code, String message, HttpStatus status) {
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("error", code);
            jsonObject.put("message", message);
        } catch (JSONException e) {
            return new ResponseEntity<>(JSON_ERROR, HttpStatus.BAD_REQUEST);
        }
        return new ResponseEntity<>(jsonObject.toString(), status);
    }

    public static ResponseEntity<String> getTotalRank(Integer personId, Long rank) {
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("person", personId);
            if (rank == null)
                rank = 0L;
            jsonObject.put("rank", rank);
        } catch (JSONException e) {
            return new ResponseEntity<>(JSON_ERROR, HttpStatus.BAD_REQUEST);
        }
        return new ResponseEntity<>(jsonObject.toString(), HttpStatus.OK);
    }

    public static ResponseEntity<String> getTotalRank(Integer personId, Integer siteId,
                                                      Long rank) {
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("person", personId);
            jsonObject.put("site", siteId);
            if (rank == null)
                rank = 0L;
            jsonObject.put("rank", rank);
        } catch (JSONException e) {
            return new ResponseEntity<>(JSON_ERROR, HttpStatus.BAD_REQUEST);
        }
        return new ResponseEntity<>(jsonObject.toString(), HttpStatus.OK);
    }
}
