package ru.tac.anystat.javarest.model;

import com.fasterxml.jackson.annotation.*;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.io.Serializable;
import java.sql.Timestamp;
import java.util.Set;

/**
 * Created by ustits on 02.10.16.
 */
@Entity
@Table(name = "Pages")
public class Page implements Serializable {

    private static final long serialVersionUID = -12382404;

    private Integer id;
    private String url;
    @JsonIdentityInfo(generator=ObjectIdGenerators.PropertyGenerator.class, property="id")
    @JsonIdentityReference(alwaysAsId=true)
    private Site site;
    private Timestamp foundDateTime;
    private Timestamp lastScanDate;
    private Timestamp modificationDate;

    @JsonIgnore
    private Set<PersonPageRank> ranks;

    public Page() {
    }

    public Page(Page value) {
        this.id = value.id;
        this.url = value.url;
        this.site = value.site;
        this.foundDateTime = value.foundDateTime;
        this.lastScanDate = value.lastScanDate;
        this.modificationDate = value.getModificationDate();
    }

    public Page(
            Integer id,
            String url,
            Site site,
            Timestamp foundDateTime,
            Timestamp lastScanDate,
            Timestamp modificationDate
    ) {
        this.id = id;
        this.url = url;
        this.site = site;
        this.foundDateTime = foundDateTime;
        this.lastScanDate = lastScanDate;
        this.modificationDate = modificationDate;
    }

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "ID", unique = true, nullable = false, precision = 10)
    public Integer getId() {
        return this.id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    @Column(name = "URL", nullable = false, length = 2048)
    @NotNull
    @Size(max = 2048)
    public String getUrl() {
        return this.url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    @ManyToOne
    @JoinColumn(name = "SiteID")
    public Site getSite() {
        return this.site;
    }

    public void setSite(Site site) {
        this.site = site;
    }

    @Column(name = "FoundDateTime", nullable = false)
    @NotNull
    public Timestamp getFoundDateTime() {
        return this.foundDateTime;
    }

    public void setFoundDateTime(Timestamp foundDateTime) {
        this.foundDateTime = foundDateTime;
    }

    @Column(name = "LastScanDate")
    public Timestamp getLastScanDate() {
        return this.lastScanDate;
    }

    public void setLastScanDate(Timestamp lastScanDate) {
        this.lastScanDate = lastScanDate;
    }

    @OneToMany(cascade = CascadeType.ALL, mappedBy = "page")
    public Set<PersonPageRank> getRanks() {
        return ranks;
    }

    public void setRanks(Set<PersonPageRank> ranks) {
        this.ranks = ranks;
    }

    @Column(name = "LastModificationDate")
    public Timestamp getModificationDate() {
        return modificationDate;
    }

    public void setModificationDate(Timestamp modificationDate) {
        this.modificationDate = modificationDate;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder("Page (");

        sb.append(id);
        sb.append(", ").append(url);
        sb.append(", ").append(site.getId());
        sb.append(", ").append(foundDateTime);
        sb.append(", ").append(lastScanDate);
        sb.append(", ").append(modificationDate);

        sb.append(")");
        return sb.toString();
    }
}
