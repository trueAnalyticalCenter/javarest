package ru.tac.anystat.javarest.model;

import com.fasterxml.jackson.annotation.*;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.io.Serializable;
import java.util.Set;

/**
 * Created by ustits on 02.10.16.
 */
@Entity
@Table(name = "Users")
public class User implements Serializable {

    private static final long serialVersionUID = -1913955668;

    private Integer id;
    private String username;
    private String password;
    private Boolean isAdmin;
    @JsonIdentityInfo(generator=ObjectIdGenerators.PropertyGenerator.class, property="id")
    @JsonIdentityReference(alwaysAsId=true)
    private User createdBy;

    @JsonIgnore
    private Set<Site> sites;
    @JsonIgnore
    private Set<Person> persons;
    @JsonIgnore
    private Set<User> users;

    public User() {
    }

    public User(Integer id) {
        this.id = id;
    }

    public User(User value) {
        this.id = value.id;
        this.username = value.username;
        this.password = value.password;
        this.isAdmin = value.isAdmin;
        this.createdBy = value.createdBy;
    }

    public User(
            Integer id,
            String username,
            String password,
            Boolean isAdmin,
            User createdBy
    ) {
        this.id = id;
        this.username = username;
        this.password = password;
        this.isAdmin = isAdmin;
        this.createdBy = createdBy;
    }

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "ID", unique = true, nullable = false, precision = 10)
    public Integer getId() {
        return this.id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    @Column(name = "Username", nullable = false, length = 512)
    @NotNull
    @Size(max = 512)
    public String getUsername() {
        return this.username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    @Column(name = "Password", nullable = false, length = 32, columnDefinition = "char")
    @NotNull
    @Size(max = 32)
    public String getPassword() {
        return this.password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    @Column(name = "isAdmin", nullable = false, precision = 3)
    public Boolean getIsAdmin() {
        return this.isAdmin;
    }

    public void setIsAdmin(Boolean isAdmin) {
        this.isAdmin = isAdmin;
    }

    @ManyToOne
    @JoinColumn(name = "createdByAdminId")
    public User getCreatedBy() {
        return this.createdBy;
    }

    public void setCreatedBy(User createdBy) {
        this.createdBy = createdBy;
    }

    @OneToMany(cascade = CascadeType.ALL, mappedBy = "addedBy")

    public Set<Site> getSites() {
        return sites;
    }

    public void setSites(Set<Site> sites) {
        this.sites = sites;
    }

    @OneToMany(cascade = CascadeType.ALL, mappedBy = "addedBy")
    public Set<Person> getPersons() {
        return persons;
    }

    @OneToMany(cascade = CascadeType.ALL, mappedBy = "createdBy")
    public Set<User> getUsers() {
        return users;
    }

    public void setUsers(Set<User> users) {
        this.users = users;
    }

    public void setPersons(Set<Person> persons) {
        this.persons = persons;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder("User (");

        sb.append(id);
        sb.append(", ").append(username);
        sb.append(", ").append(password);
        sb.append(", ").append(isAdmin);
        sb.append(", ").append(createdBy);

        sb.append(")");
        return sb.toString();
    }
}
