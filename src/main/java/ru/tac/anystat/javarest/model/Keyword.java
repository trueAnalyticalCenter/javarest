package ru.tac.anystat.javarest.model;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.JsonIdentityReference;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.io.Serializable;

/**
 * Created by ustits on 02.10.16.
 */
@Entity
@Table(name = "Keywords")
public class Keyword implements Serializable {

    private static final long serialVersionUID = -1510578676;

    private Integer id;
    private String name;
    @JsonIdentityInfo(generator=ObjectIdGenerators.PropertyGenerator.class, property="id")
    @JsonIdentityReference(alwaysAsId=true)
    private Person person;

    public Keyword() {
    }

    public Keyword(Keyword value) {
        this.id = value.id;
        this.name = value.name;
        this.person = value.person;
    }

    public Keyword(Integer id, String name, Person person) {
        this.id = id;
        this.name = name;
        this.person = person;
    }

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "ID", unique = true, nullable = false, precision = 10)
    public Integer getId() {
        return this.id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    @Column(name = "Name", nullable = false, length = 2048)
    @NotNull
    @Size(max = 2048)
    public String getName() {
        return this.name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @ManyToOne
    @JoinColumn(name = "PersonID")
    public Person getPerson() {
        return this.person;
    }

    public void setPerson(Person person) {
        this.person = person;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder("Keyword (");

        sb.append(id);
        sb.append(", ").append(name);
        sb.append(", ").append(person.getId());

        sb.append(")");
        return sb.toString();
    }
}
