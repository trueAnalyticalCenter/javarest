package ru.tac.anystat.javarest.model;

import com.fasterxml.jackson.annotation.*;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.io.Serializable;
import java.util.Set;

/**
 * Created by ustits on 02.10.16.
 */
@Entity
@Table(name = "Persons")
public class Person implements Serializable {

    private static final long serialVersionUID = -263651962;

    private Integer id;
    private String name;
    @JsonIdentityInfo(generator=ObjectIdGenerators.PropertyGenerator.class, property="id")
    @JsonIdentityReference(alwaysAsId=true)
    private User addedBy;

    @JsonIgnore
    private Set<Keyword> keywords;
    @JsonIgnore
    private Set<PersonPageRank> ranks;

    public Person() {
    }

    public Person(Person value) {
        this.id = value.id;
        this.name = value.name;
        this.addedBy = value.addedBy;
    }

    public Person(Integer id, String name, User createdBy) {
        this.id = id;
        this.name = name;
        this.addedBy = createdBy;
    }

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "ID", unique = true, nullable = false, precision = 10)
    public Integer getId() {
        return this.id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    @Column(name = "Name", nullable = false, length = 2048)
    @NotNull
    @Size(max = 2048)
    public String getName() {
        return this.name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @ManyToOne
    @JoinColumn(name = "AddedByAdminId")
    public User getAddedBy() {
        return this.addedBy;
    }

    public void setAddedBy(User addedBy) {
        this.addedBy = addedBy;
    }

    @OneToMany(cascade = CascadeType.ALL, mappedBy = "person")
    public Set<Keyword> getKeywords() {
        return keywords;
    }

    public void setKeywords(Set<Keyword> keywords) {
        this.keywords = keywords;
    }

    @OneToMany(cascade = CascadeType.ALL, mappedBy = "person")
    public Set<PersonPageRank> getRanks() {
        return ranks;
    }

    public void setRanks(Set<PersonPageRank> ranks) {
        this.ranks = ranks;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder("Person (");

        sb.append(id);
        sb.append(", ").append(name);
        sb.append(", ").append(addedBy.getId());

        sb.append(")");
        return sb.toString();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Person person = (Person) o;

        return name.equals(person.name);
    }

    @Override
    public int hashCode() {
        return name.hashCode();
    }
}
