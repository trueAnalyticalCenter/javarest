package ru.tac.anystat.javarest.model;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.JsonIdentityReference;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.io.Serializable;

/**
 * Created by ustits on 02.10.16.
 */
@Entity
@Table(name = "PersonPageRank", uniqueConstraints = {
        @UniqueConstraint(columnNames = {"PersonID", "PageID"})})
public class PersonPageRank implements Serializable {

    private static final long serialVersionUID = 1473185251;

    private Integer id;
    @JsonIdentityInfo(generator=ObjectIdGenerators.PropertyGenerator.class, property="id")
    @JsonIdentityReference(alwaysAsId=true)
    private Person person;
    @JsonIdentityInfo(generator=ObjectIdGenerators.PropertyGenerator.class, property="id")
    @JsonIdentityReference(alwaysAsId=true)
    private Page page;
    private Integer rank;

    public PersonPageRank() {
    }

    public PersonPageRank(PersonPageRank value) {
        this.person = value.person;
        this.page = value.page;
        this.rank = value.rank;
    }

    public PersonPageRank(Integer id, Person person, Page page, Integer rank) {
        this.id = id;
        this.person = person;
        this.page = page;
        this.rank = rank;
    }

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "ID", unique = true, nullable = false, precision = 10)
    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    @ManyToOne
    @JoinColumn(name = "PersonID")
    public Person getPerson() {
        return this.person;
    }

    public void setPerson(Person person) {
        this.person = person;
    }

    @ManyToOne
    @JoinColumn(name = "PageID")
    public Page getPage() {
        return this.page;
    }

    public void setPage(Page page) {
        this.page = page;
    }

    @Column(name = "Rank", nullable = false, precision = 10)
    @NotNull
    public Integer getRank() {
        return this.rank;
    }

    public void setRank(Integer rank) {
        this.rank = rank;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder("PersonPageRank (");

        sb.append(person.getId());
        sb.append(", ").append(page.getId());
        sb.append(", ").append(rank);

        sb.append(")");
        return sb.toString();
    }
}
