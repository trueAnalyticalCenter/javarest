package ru.tac.anystat.javarest.model;

import com.fasterxml.jackson.annotation.*;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.io.Serializable;
import java.util.Set;

/**
 * Created by ustits on 27.09.16.
 */
@Entity
@Table(name = "Sites")
public class Site implements Serializable {

    private static final long serialVersionUID = 1779682612;

    private Integer id;
    private String name;
//    @JsonIgnoreProperties({"username", "password", "isAdmin", "createdBy" })
    @JsonIdentityInfo(generator=ObjectIdGenerators.PropertyGenerator.class, property="id")
    @JsonIdentityReference(alwaysAsId=true)
    private User addedBy;

//    public Integer userId;

    @JsonIgnore
    private Set<Page> pages;

    public Site() {
    }

    public Site(Site value) {
        this.id = value.id;
        this.name = value.name;
        this.addedBy = value.addedBy;
    }

    public Site(Integer id) { this.id = id; }

    public Site(Integer id, String name, User addedBy) {
        this.id = id;
        this.name = name;
        this.addedBy = addedBy;
    }

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "ID", unique = true, nullable = false, precision = 10)
    public Integer getId() {
        return this.id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    @Column(name = "Name", nullable = false, length = 256)
    @NotNull
    @Size(max = 256)
    public String getName() {
        return this.name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @ManyToOne
    @JoinColumn(name = "AddedByAdminId")
    public User getAddedBy() {
        return this.addedBy;
    }

    public void setAddedBy(User addedBy) {
        this.addedBy = addedBy;
    }

    @OneToMany(cascade = CascadeType.ALL, mappedBy = "site")
    public Set<Page> getPages() {
        return pages;
    }

    public void setPages(Set<Page> pages) {
        this.pages = pages;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder("Site (");

        sb.append(id);
        sb.append(", ").append(name);
        sb.append(", ").append(addedBy.getId());

        sb.append(")");
        return sb.toString();
    }
}
