FROM ustits/javarest:latest
ADD target/javarest.jar javarest.jar
ENTRYPOINT ["java", "-jar", "javarest.jar"]