# JavaREST README #

### What is this repository for? ###

* Communicate with MySQL 
* Give you JSON

###Wiki###
* [Basic info](https://bitbucket.org/trueAnalyticalCenter/javarest/wiki/Basic)
* [Version info](https://bitbucket.org/trueAnalyticalCenter/javarest/wiki/Versions%20info)
* [Contacts](https://bitbucket.org/trueAnalyticalCenter/javarest/wiki/Contacts)
### How to run ###
Install jdk 1.8, package sources and run:

```
#!bash

java -jar javarest.jar
```

### Used frameworks ###
* Spring Data
* Spring Boot
* jOOQ pojo generator
###Version###
1.3.0